import pytest
import sys, os
testdir = os.path.dirname(__file__)
srcdir = '../sad/'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

from admin import Admin
from listing import Listing
from advertiser import Advertiser

adverts = [Advertiser(0,0,0,0,0,0,0,0,0), Advertiser(0,0,0,0,0,0,0,0,0)]

def test_addListing_lenplus1(listings):
    newlisting = [Listing(0,0,0,0,0,0,0,0,0,0)]
    currentlen = len(listings)
    Admin.addListing(listings, newlisting)
    newlen = len(listings)
    assert currentlen + 1 == newlen

def test_addAdvertiser_lenplus1(listings):
    adverts = [Advertiser(0,0,0,0,0,0,0,0,0), Advertiser(0,0,0,0,0,0,0,0,0)]
    currentlen = len(adverts)
    newadvert = [Advertiser(0,0,0,0,0,0,0,0,0)]
    Admin.addAdvertiser(adverts, newadvert)
    newlen = len(adverts)
    assert currentlen + 1 == newlen

def test_alterListing_price(listings):
    Admin.alterListing(listings, "400", "/admin/altlist/price/1")
    assert listings[0].price == "400"

def test_alterListing_description(listings):
    Admin.alterListing(listings, "400", "/admin/altlist/description/1")
    assert listings[0].description == "400"

def test_alterListing_image(listings):
    Admin.alterListing(listings, "400", "/admin/altlist/image/1")
    assert listings[0].image == "400"

def test_alterListing_link(listings):
    Admin.alterListing(listings, "400", "/admin/altlist/link/1")
    assert listings[0].link == "400"

def test_alterListing_category(listings):
    Admin.alterListing(listings, "400", "/admin/altlist/category/1")
    assert listings[0].category == "400"

def test_alterListing_featured(listings):
    Admin.alterListing(listings, "400", "/admin/altlist/featured/1")
    assert listings[0].featured == "400"

def test_alterListing_sdate(listings):
    Admin.alterListing(listings, "400", "/admin/altlist/sdate/1")
    assert listings[0].sdate == "400"

def test_alterListing_edate(listings):
    Admin.alterListing(listings, "400", "/admin/altlist/edate/1")
    assert listings[0].edate == "400"

def test_addAdvertiser_email():
    Admin.alterAdvertiser(adverts, "400", "/admin/altadvert/email/1")
    assert adverts[0].email == "400"

def test_addAdvertiser_lastname():
    Admin.alterAdvertiser(adverts, "400", "/admin/altadvert/lastname/1")
    assert adverts[0].lastname == "400"

def test_addAdvertiser_business():
    Admin.alterAdvertiser(adverts, "400", "/admin/altadvert/business/1")
    assert adverts[0].business == "400"

def test_addAdvertiser_phone():
    Admin.alterAdvertiser(adverts, "400", "/admin/altadvert/phone/1")
    assert adverts[0].phone == "400"

def test_addAdvertiser_facebook():
    Admin.alterAdvertiser(adverts, "400", "/admin/altadvert/facebook/1")
    assert adverts[0].facebook == "400"

def test_addAdvertiser_twitter():
    Admin.alterAdvertiser(adverts, "400", "/admin/altadvert/twitter/1")
    assert adverts[0].twitter == "400"

def test_addAdvertiser_linkedin():
    Admin.alterAdvertiser(adverts, "400", "/admin/altadvert/linkedin/1")
    assert adverts[0].linkedin == "400"

def test_addAdvertiser_addlisting():
    Admin.alterAdvertiser(adverts, "400", "/admin/altadvert/addlistings/1")
    assert str(adverts[0].listings) == "0 400"

def test_addAdvertiser_fulllistings():
    Admin.alterAdvertiser(adverts, "0 2 400", "/admin/altadvert/fulllistings/1")
    assert str(adverts[0].listings) == "0 2 400"


