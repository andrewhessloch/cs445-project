import pytest
import sys, os
testdir = os.path.dirname(__file__)
srcdir = '../sad/'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

from cslookup import CSLookup
from advertiser import Advertiser

adverts = [Advertiser(0,0,0,0,0,0,0,2,0), Advertiser(0,0,0,0,0,0,0,0,0)]

def test_lookupAdverts():
    assert adverts[0] == CSLookup.lookupAdverts(adverts, "2")[0]

def test_lookupListingByID(listings):
    assert CSLookup.lookupListingByID(listings, "2")[0] == listings[1] 
