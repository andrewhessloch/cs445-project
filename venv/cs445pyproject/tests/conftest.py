import pytest, sys, os
testdir = os.path.dirname(__file__)
srcdir = '../sad/'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

from listing import Listing
from datahandler import DataHandler

import pytest

@pytest.fixture
def listings():
    handler = DataHandler()
    listings = handler.handleListingsXML(open('listings.xml').read())
    return listings
