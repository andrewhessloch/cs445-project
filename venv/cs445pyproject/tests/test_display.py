import pytest, sys, os
testdir = os.path.dirname(__file__)
srcdir = '../sad/'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

from display import Display

def test_getCategories(listings):
    assert Display.getCategories(listings, '/sad') == ['birthday', 'family', 'fun']

def test_getlistingsOfCategory(listings):
    assert Display.getListingsOfCategory(listings, "/sad/birthday")[0] == listings[0]

def test_getFeaturedListings(listings):
    assert Display.getFeaturedListings(listings, "p", "/sad")[0] == listings[1] 
