import pytest, sys, os
testdir = os.path.dirname(__file__)
srcdir = '../sad/'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

from advertiser import Advertiser
from listing import Listing
from datahandler import DataHandler

handler = DataHandler()
def test_handleListingsXML():
    result = handler.handleListingsXML(open('listings.xml').read())
    assert type(result) == type([Listing(0,1,2,3,4,5,6,7,8,9), Listing(0,1,2,3,4,5,6,7,8,9)])

def test_handleListingsJSON():
    result = handler.handleListingsJSON(open('listings.json').read())
    assert type(result) == type([Listing(0,1, 2, 3, 4, 5, 6, 7, 8, 9), Listing(0,1,2,3,4,5,6,7,8,9)])

def test_handleAdvertisersXML():
    result = handler.handleAdvertisersXML(open('advertisers.xml').read())
    assert type(result) == type([Advertiser(1, 2, 3, 4, 5, 6, 7, 8, 9), Advertiser(1,2,3,4,5,6,7,8,9)])

def test_handleAdvertisersJSON():
    result = handler.handleAdvertisersJSON(open('advertisers.json').read())
    assert type(result) == type([Advertiser(1, 2, 3, 4, 5, 6, 7, 8, 9), Advertiser(1,2,3,4,5,6,7,8,9)])
