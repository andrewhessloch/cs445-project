from listing import Listing
from advertiser import Advertiser
import xml.etree.ElementTree as etree
import json
from pprint import pprint

class DataHandler:
    def handleListingsXML(self, xmlfile):
        with open("output.txt", "w") as text_file:
            try:
                text_file.write(xmlfile.decode("utf-8"))
            except AttributeError:
                text_file.write(xmlfile)
                
        tree = etree.parse("output.txt")
        root = tree.getroot()
        listings = []
        for listing in root:
            ident = listing[0].text
            description = listing[1].text
            image = listing[2].text
            link = listing[3].text
            category = listing[4].text
            featured = listing[5].text
            advertiser = listing[6].text
            sdate = listing[7].text
            edate = listing[8].text
            price = listing[9].text
            listings.append(Listing(ident, description, image, link, category, featured, advertiser, sdate, edate, price))
        return listings

    def handleListingsJSON(self, jsonfile):
        with open("output.txt", "w") as text_file:
            try:
                text_file.write(jsonfile.decode("utf-8"))
            except:
                text_file.write(jsonfile)
                
        listings = []
        with open('output.txt', 'r') as data_file:
            data = json.load(data_file)
            for listing in data:
                ident = listing["ident"]
                description = listing["description"]
                image = listing["image"]
                link = listing["link"]
                category = listing["category"]
                featured = listing["featured"]
                advertiser = listing["advertiser"]
                sdate = listing["sdate"]
                edate = listing["edate"]
                price = listing["price"]
                listings.append(Listing(ident, description, image, link, category, featured, advertiser, sdate, edate, price))
        return listings

    def handleAdvertisersXML(self, xmlfile):
        with open("output.txt", "w") as text_file:
            try:
                text_file.write(xmlfile.decode("utf-8"))
            except:
                text_file.write(xmlfile)
                
        tree = etree.parse("output.txt")
        root = tree.getroot()
        advertisers = []
        for advertiser in root:
            ident = advertiser[0].text
            email = advertiser[1].text
            lastname = advertiser[2].text
            business = advertiser[3].text
            phone = advertiser[4].text
            facebook = advertiser[5].text
            twitter = advertiser[6].text
            linkedin = advertiser[7].text
            listings = advertiser[8].text
            advertisers.append(Advertiser(ident, email, lastname, business, phone, facebook, twitter, linkedin, listings))
        return advertisers

    def handleAdvertisersJSON(self, jsonfile):
        with open("output.txt", "w") as text_file:
            try:
                text_file.write(jsonfile.decode("utf-8"))
            except AttributeError:
                text_file.write(jsonfile)
                
        advertisers = []
        with open("output.txt") as data_file:
            data = json.load(data_file)
        for advertiser in data:
            ident = advertiser["ident"]
            email = advertiser["email"]
            lastname = advertiser["lastname"]
            business = advertiser["business"]
            phone = advertiser["phone"]
            facebook = advertiser["facebook"]
            twitter = advertiser["twitter"]
            linkedin = advertiser["linkedin"]
            listings = advertiser["listings"]
                
            advertisers.append(Advertiser(ident, email, lastname, business, phone, facebook, twitter, linkedin, listings))
        return advertisers
