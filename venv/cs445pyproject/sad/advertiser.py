class Advertiser():
    def __init__(self, ident, email, lastname, business, phone, facebook, twitter, linkedin, listings):
        self.ident = ident
        self.email = email
        self.lastname = lastname
        self.business = business
        self.phone = phone
        self.facebook = facebook
        self.twitter = twitter
        self.linkedin = linkedin
        self.listings = listings
