class CSLookup():
    def lookupAdverts(advertisers, search):
        results = []
        for advert in advertisers:
            if search in str(advert.ident):
                results.append(advert)
            elif search in str(advert.email):
                results.append(advert)
            elif search in str(advert.lastname):
                results.append(advert)
            elif search in str(advert.business):
                results.append(advert)
            elif search in str(advert.phone):
                results.append(advert)
            elif search in str(advert.facebook):
                results.append(advert)
            elif search in str(advert.twitter):
                results.append(advert)
            elif search in str(advert.linkedin):
                results.append(advert)
        return results

    def lookupListingByID(listings, search):
        try:
            results = [listings[int(search) - 1]]
        except:
            results = [listings[0]]
        return results
