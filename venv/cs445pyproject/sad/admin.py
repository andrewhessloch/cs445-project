from listing import Listing
from cslookup import CSLookup
import json
import xml.etree.ElementTree as etree

class Admin:
    def addListing(origlistingsarray, tbalistingsarray):
        for listing in tbalistingsarray:
            origlistingsarray.append(listing)

    def addAdvertiser(origadvertsarray, tbaadvertsarray):
        for advert in tbaadvertsarray:
            origadvertsarray.append(advert)
    
    def alterListing(listings, newvalue, path):
        hvars = path.replace('/admin/altlist/', '').replace('/', ' ').split()
        attr = hvars[0]
        ident = int(hvars[1]) - 1
        if(attr == 'description'):
            listings[ident].description = newvalue
        elif(attr == 'image'):
            listings[ident].image = newvalue
        elif(attr == 'link'):
            listings[ident].link = newvalue
        elif(attr == 'category'):
            listings[ident].category = newvalue
        elif(attr == 'featured'):
            listings[ident].featured = newvalue
        elif(attr == 'advertiser'):
            listings[ident].advertiser = newvalue
        elif(attr == 'sdate'):
            listings[ident].sdate = newvalue
        elif(attr == 'edate'):
            listings[ident].edate = newvalue
        elif(attr == 'price'):
            listings[ident].price = newvalue

            
    def alterAdvertiser(advertisers, newvalue, path):
        hvars = path.replace('/admin/altadvert/', '').replace('/', ' ').split()
        attr = hvars[0]
        ident = int(hvars[1]) - 1
        if(attr == 'email'):
            advertisers[ident].email = newvalue
        elif(attr == 'lastname'):
            advertisers[ident].lastname = newvalue
        elif(attr == 'business'):
            advertisers[ident].business = newvalue
        elif(attr == 'phone'):
            advertisers[ident].phone = newvalue
        elif(attr == 'facebook'):
            advertisers[ident].facebook = newvalue
        elif(attr == 'twitter'):
            advertisers[ident].twitter = newvalue
        elif(attr == 'linkedin'):
            advertisers[ident].linkedin = newvalue
        elif(attr == 'addlistings'):
            advertisers[ident].listings = str(advertisers[ident].listings) + ' ' + str(newvalue)
        elif(attr == 'fulllistings'):
            advertisers[ident].listings = str(newvalue)
        
    def saveListings(listings):
        with open("listings.json", "w") as text_file:
            text_file.write(json.dumps([ob.__dict__ for ob in listings], indent=3, sort_keys=True))
            

    def saveAdvertisers(advertisers):
        with open("listings.json", "w") as text_file:
            text_file.write(json.dumps([ob.__dict__ for ob in advertisers], indent=3, sort_keys=True))
