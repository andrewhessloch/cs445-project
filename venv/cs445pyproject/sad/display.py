import re, random, datetime
from listing import Listing

class Display:
    def getCategories(listings, path):
        categories = []
        path1 = path.replace('/sad/', '')
        path2 = path.replace('/sad', '')
        regex = re.escape(path)
        
        if(path2 == ''):
            for listing in listings:
                catstring = listing.category.split()
                for category in catstring:
                    categories.append(re.findall('(?<=\/)(.*?)(?=\/)', '/' + re.sub(regex, '', category) + '/')[0])
        else:
            path = path1
            for listing in listings:
                catstring = listing.category.split()
                for category in catstring:
                    if path + '/' in category:
                        categories.append(re.findall('(?<=\/)(.*?)(?=\/)', re.sub(regex, '', category).replace(path, '') + '/')[0])
        categories = sorted(list(set(categories)))
        return categories

    def getListingsOfCategory(listings, catstring):
        catlistings = []
        for listing in listings:
            catstring = catstring.replace('/sad/','')
            cdate = datetime.datetime.now().strftime("%Y %m %d").split()
            edate = listing.edate.split()
            if(catstring in listing.category):
                if(listing.edate == 'N/A'):
                    catlistings.append(listing)
                else:
                    if(int(cdate[0])*365 + int(cdate[1])*30 + int(cdate[2]) < int(edate[0])*30 + int(edate[1]) + int(edate[2])*365):
                        catlistings.append(listing)
        return catlistings

    def getFeaturedListings(listings, featuredby, path):
        featuredlist = []
        if(path.replace('/sad', '') == ''):
            for listing in listings:
                if(listing.featured == 'sad'):
                    featuredlist.append(listing)
        else:
            path = path.replace('/sad', '')
            check = path.split('/')[-1]
            for listing in listings:
                if(listing.featured == check):
                    featuredlist.append(listing)

        if(featuredby == 'r'):
            random.shuffle(featuredlist)
        elif(featuredby == 'a'):
            featuredlist.sort(key=lambda x: x.advertiser)
        elif(featuredby == 'p'):
            featuredlist.sort(key=lambda x: x.price, reverse=True)
        elif(featuredby == 'd'):
            featuredlist.sort(key=lambda x: (x.sdate.split()[2] + x.sdate.split()[1] + x.sdate.split()[0]).replace(' ', ''), reverse=True)
        return featuredlist
        
