from display import Display
from listing import Listing
from datahandler import DataHandler
from admin import Admin
from cslookup import CSLookup
import json, cgi

from http.server import BaseHTTPRequestHandler

handler = DataHandler()
listings = handler.handleListingsJSON(open('listings.json').read())
advertisers = handler.handleAdvertisersJSON(open('advertisers.json').read())
featuredby = 'r'

class MyHTTPHandler(BaseHTTPRequestHandler):        
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        path = self.path
        if('/sad' in path):
            testpath1 = path.replace('/sad', ' ')
            testpath2 = path.replace('/sad/', ' ')
            if(testpath1 == ' ' or testpath2 == ' '):
                self.wfile.write(bytes('categories:', 'utf-8'))
                for cat in Display.getCategories(listings, path):
                    self.wfile.write(bytes(' ', 'utf-8'))
                    self.wfile.write(bytes(cat, 'utf-8'))
                self.wfile.write(bytes('\n', 'utf-8'))

                self.wfile.write(bytes('/////FEATURED/////\n', 'utf-8'))
                self.wfile.write(bytes(json.dumps([ob.__dict__ for ob in Display.getFeaturedListings(listings, featuredby, path)], indent=3, sort_keys=True), 'utf-8'))
                self.wfile.write(bytes('/////END FEATURED/////\n', 'utf-8'))
                self.wfile.write(bytes(json.dumps([ob.__dict__ for ob in listings], indent=3, sort_keys=True), 'utf-8'))  
            else:
                currentListings = Display.getListingsOfCategory(listings, path)
                self.wfile.write(bytes('sub-categories:', 'utf-8'))
                for cat in Display.getCategories(listings, path):
                    self.wfile.write(bytes(' ', 'utf-8'))
                    self.wfile.write(bytes(cat, 'utf-8'))
                self.wfile.write(bytes('\n', 'utf-8'))
                self.wfile.write(bytes('/////FEATURED/////\n', 'utf-8'))
                self.wfile.write(bytes(json.dumps([ob.__dict__ for ob in Display.getFeaturedListings(currentListings, featuredby, path)], indent=3, sort_keys=True), 'utf-8'))
                self.wfile.write(bytes('/////END FEATURED/////\n', 'utf-8'))
                self.wfile.write(bytes(json.dumps([ob.__dict__ for ob in currentListings], indent=3, sort_keys=True), 'utf-8'))
        elif('/cs' in path):
            testpath1 = path.replace('/cs', ' ')
            testpath2 = path.replace('/cs/', ' ')
            if(testpath1 == ' ' or testpath2 == ' '):
                self.wfile.write(bytes(json.dumps([ob.__dict__ for ob in advertisers], indent=3, sort_keys=True), 'utf-8'))
            elif('/cs/search/' in path):
                search = path.replace('/cs/search/', '')
                self.wfile.write(bytes(json.dumps([ob.__dict__ for ob in CSLookup.lookupAdverts(advertisers,search)], indent=3, sort_keys=True), 'utf-8'))
            elif('/cs/listing/' in path):
                search = path.replace('/cs/listing/', '')
                self.wfile.write(bytes(json.dumps([ob.__dict__ for ob in CSLookup.lookupListingByID(listings, search)], indent=3, sort_keys=True), 'utf-8'))
            else:
                self.wfile.write(bytes("Not a valid URL\n", 'utf-8'))
                
    def do_POST(self):
        path = self.path
        if(path == '/admin/addl/json'):
            print("Add JSON")
            length = int(self.headers['Content-Length'])
            postvars = handler.handleListingsJSON(self.rfile.read(length))
            Admin.addListing(listings, postvars)
            Admin.saveListings(listings)

        elif(path == '/admin/addl/xml'):
            print("Add XML")
            length = int(self.headers['Content-Length'])
            postvars = handler.handleListingsXML(self.rfile.read(length))
            Admin.addListing(listings, postvars)
            Admin.saveListings(listings)

        elif(path == '/admin/adda/xml'):
            length = int(self.headers['Content-Length'])
            postvars = handler.handleAdvertisersXML(self.rfile.read(length))
            Admin.addAdvertiser(advertisers, postvars)
            Admin.saveAdvertisers(advertisers)

        elif(path == '/admin/fchange'):
            global featuredby
            length = int(self.headers['Content-Length'])
            postvars = self.rfile.read(length)            
            featuredby = postvars.decode('utf-8')

        elif('/admin/altlist' in path):
            length = int(self.headers['Content-Length'])
            postvars = self.rfile.read(length).decode('utf-8')
            Admin.alterListing(listings, postvars, path)
            Admin.saveListings(listings)

        elif(path == '/admin/altadvert'):
            length = int(self.headers['Content-Length'])
            postvars = self.rfile.read(length).decode('utf-8')
            Admin.alterAdvertiser(listings, postvars, path)
            Admin.saveAdvertisers(advertisers)
        
        else:
            self.wfile.write(bytes("Not a valid URL\n".encode("utf-8")))
