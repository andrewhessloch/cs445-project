class Listing:
    def __init__(self, ident, description, image, link, category, featured, advertiser, sdate, edate, price):
        self.ident = ident
        self.price = price
        self.sdate = sdate
        self.edate = edate
        self.featured = featured
        self.description = description
        self.link = link
        self.image = image
        self.category = category
        self.advertiser = advertiser
